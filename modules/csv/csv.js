const fs = require('fs');
const csvjson = require('csvjson');
const mysql = require('../mysql');

let csvOptions = {

}

exports.init = function() {
  this.checkForNew();
}

exports.checkForNew = function() {
  if (app.config.csv_import.type == 'dir') {
    fs.readdirSync(app.config.csv_import.dir).map((filename) => {
      if (fs.statSync(app.config.csv_import.dir + '/' + filename).isFile()) {
        let tmp = csvjson.toObject(fs.readFileSync(app.config.csv_import.dir + '/' + filename).toString('utf8'), csvOptions);
        if (tmp) {
          mysql.newOrders(tmp, filename);
          fs.renameSync(app.config.csv_import.dir + '/' + filename, app.config.csv_import.dir_output + '/' + filename)
        }
      }
    })
  }
}
