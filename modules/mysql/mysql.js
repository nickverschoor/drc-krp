const mysql = require('mysql');
const Promise = require('promise');
let connection1 = null;
let connection2 = null;


exports.init = function() {
  connection1 = mysql.createConnection({
    host: app.config.database[0].host,
    user: app.config.database[0].user,
    password: app.config.database[0].pass,
    database: app.config.database[0].database
  });
  connection2 = mysql.createConnection({
    host: app.config.database[1].host,
    user: app.config.database[1].user,
    password: app.config.database[1].pass,
    database: app.config.database[1].database
  });
}

exports.getOrders = function(cb) {
  connection1.query('SELECT * FROM order_overview ORDER BY order_id DESC', (err, rows1) => {
    connection2.query('SELECT * FROM order_overview', (err, rows2) => {
      let orders = rows1.map(row => {
        let confirmedOrder = search('order_id', row.order_id, rows2);
        let tmp = {
          orderNumber: row.order_id,
          timestamp: row.input_date,
          pages: row.total_document_pages,
          info: confirmedOrder ? (row.total_document_pages == confirmedOrder.total_document_pages ? 'OK' : 'PAGE_MISMATCH') : 'NO_CONFIRMATION'
        }
        return tmp;
      })
      cb(orders)
    })
  })
}

exports.newOrders = function(array, filename) {
  console.log(array);
  let insertId = 1;
  connection2.query('INSERT INTO order_confirmation SET ?', {
    name: filename,
    content: JSON.stringify(array),
    status: 'OK'
  }, (error, result, fields) => insertId = result.insertId);
  array.map((cur) => {
    try {
      connection2.query('INSERT INTO order_overview SET ?', {
        order_id: parseInt(String(cur.OfficeMailOrderNummer).replace(/\D/g, '')),
        total_document_pages: parseInt(cur.AantalPaginas)
      }, (error, results, fields) => {
        if (error) console.log('ERROR: ' + error + ' ' + insertId), connection2.query('UPDATE order_confirmation SET ? WHERE ?', [{
          status: error.code
        }, {
          id: insertId
        }]);
      });
    } catch (error) {
      console.log('ERR: ' + error);
    }
  })

}

function search(key, keyVal, arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i][key] === keyVal) {
      return arr[i];
    }
  }
}
