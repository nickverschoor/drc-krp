const express = require('express');
const www = express();
const compression = require('compression');
const server = require('http').Server(www);
const session = require('express-session');
const path = require('path');
const mysql = require('../mysql');
const io = require('socket.io')(server);



www.set('views', path.join(app.appRoot, 'public'));
www.set('view engine', 'ejs');
www.engine('html', require('ejs').renderFile);
www.use(session({
  secret: '6a55fe49d1251693e18f9b699719f711',
  resave: true,
  saveUninitialized: true
}));
www.use(compression());
www.use(express.static(path.join(app.appRoot, 'public')));

www.get('/', (req, res) => {
  res.render('index');
})

io.on('connection', (socket) => {
  io.to(socket.id).emit('ON_UPDATE', app.data.orders)

  socket.on('REQ_UPDATE',() => app.emit('REQ_UPDATE'));
})

app.on('ON_UPDATE', (data) => io.emit('ON_UPDATE', data))

exports.init = function() {
  server.listen(app.config.server.port, () => {
    console.log('Started web server and listening on port ' + app.config.server.port);
  });
}
