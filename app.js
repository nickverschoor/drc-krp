global.app = module.exports = {};
const Events = require('events');
app = new Events.EventEmitter();
app.appRoot = __dirname;
app.config = new Object();

const www = require('./modules/www');
const mysql = require('./modules/mysql');
const csv = require('./modules/csv');
const fs = require('fs');

app.data = {
  orders: new Object()
}

init();

app.on('ON_MINUTE_TICK', () => {
  csv.checkForNew();
  getOrdersList();
})

function init() {
  console.log('App init...');
  console.log('Looking for config...');
  if (fs.existsSync(app.appRoot + '/config.json')){
    console.log('Found config loading values...');
    try {
      app.config = JSON.parse(fs.readFileSync(app.appRoot + '/config.json'));
    } catch(err) {
      throw new Error('Error loading config! Review your config and try agian.\r\n' + err.message);
    }
    console.log('Config successfully loaded!');
  } else {
    throw new Error('No config found! Please add one and try again.');
  }

  mysql.init();
  www.init();
  csv.init();

  setInterval(() => app.emit('ON_HOUR_TICK'), 36e5);
  setInterval(() => app.emit('ON_MINUTE_TICK'), 6e4);
  setInterval(() => app.emit('ON_SECOND_TICK'), 1e3);
  setInterval(() => app.emit('ON_MS_TICK'), 1);
  getOrdersList();
}
function getOrdersList() {
  mysql.getOrders((data) => {
    app.data.orders = data;
    app.emit('ON_UPDATE', app.data.orders);
  });
}
