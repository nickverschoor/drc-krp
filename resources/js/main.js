import 'jquery';
import 'bootstrap';
import './../scss/main.scss';

import Vue from 'vue/dist/vue.esm.js';
import io from 'socket.io-client';
import VueSocketio from 'vue-socket.io';
import {VueFeatherIconsSsrJsx as icon} from 'vue-feather-icons-ssr';

let socket = io.connect('/');

Vue.use(VueSocketio, socket);
Vue.component('icon', icon);
let vm = new Vue({
  el: '#app',
  data: {
    files: new Array
  },
  sockets:{
    ON_UPDATE: function(data){
      console.log(data);
      this.files = data;
    }
  }
})

console.log(vm);
